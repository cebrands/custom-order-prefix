<?php
class Webkul_SalesPrefixer_Model_Mysql4_Prefixentitytype extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("salesprefixer/prefixentitytype", "entity_type_id");
    }
}