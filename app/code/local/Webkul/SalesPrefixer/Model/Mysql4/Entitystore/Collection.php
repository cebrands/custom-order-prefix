<?php

class Webkul_SalesPrefixer_Model_Mysql4_Entitystore_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init("salesprefixer/entitystore");
    }

}