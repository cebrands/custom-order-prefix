<?php

class Webkul_SalesPrefixer_Model_Mysql4_Entitystore extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct()
    {
        $this->_init("salesprefixer/entitystore", "entity_store_id");
    }

}