<?php

/**
 * Class Webkul_SalesPrefixer_Model_Events_Observer
 */
class Webkul_SalesPrefixer_Model_Events_Observer
{
    /**
     * Get last increment id from order/invoice/shipment/creditmemo entities.
     * @param $entityType - entity type (order/invoice/shipment/creditmemo)
     * @param $prefix - prefix from core_config_data
     * @param $storeId - changing store id
     * @return null
     */
    protected function _getLastEntityIncrementId($entityType, $prefix, $storeId)
    {
        $model = 'sales/' . $entityType;
        if ($entityType != 'order') {
            $model = 'sales/order_' . $entityType;
        }
        $lastEntity = Mage::getModel($model)->getCollection()
            ->addAttributeToFilter('store_id', $storeId)
            ->addAttributeToFilter('increment_id', array('like' => $prefix . '%'))
            ->getLastItem();

        if (!empty($lastEntity) && $lastEntity->getId()) {
            return $lastEntity->getIncrementId();
        }

        return null;
    }

    /**
     * Changing prefix for the entity.
     * @param $entityType - entity type (order/invoice/shipment/creditmemo)
     * @param $prefix - prefix from core_config_data
     * @param $store - current store
     * @return $this
     */
    protected function _changePrefix($entityType, $prefix, $store)
    {
        $type = Mage::getModel('salesprefixer/prefixentitytype')->getCollection()
            ->addFieldToFilter('entity_type_code', $entityType)
            ->getFirstItem();

        //If wrong type was provided in $entityType
        if (empty($type) || !$type->getId()) {
            return $this;
        }

        $entityStore = Mage::getModel('salesprefixer/entitystore')->getCollection()
            ->addFieldToFilter('entity_type_id', $type->getId())
            ->addFieldToFilter('store_id', $store->getId())
            ->getFirstItem();

        if (empty($entityStore) || !$entityStore->getId()) {
            $entityStore = Mage::getModel('salesprefixer/entitystore');
            $entityStore->setEntityTypeId($type->getId());
            $entityStore->setStoreId($store->getId());
        }

        $entityStore->setIncrementPrefix($prefix);
        $entityStore->setIncrementLastId(
            $this->_getLastEntityIncrementId($entityType, $prefix, $store->getId())
        );
        $entityStore->save();

        return $this;
    }

    public function changePrefixerAfterConfigSave()
    {
        $data = Mage::app()->getRequest()->getParams();

        if ($data['section'] != 'salesprefixer') {
            return;
        }

        /**
         * Order prefix can be set only on store level, otherwise we could have bugs with prefixes.
         */
        if (!isset($data["store"]) || !$data["store"]) {
            return;
        }

        $store = Mage::getModel("core/store")->load($data["store"]);

        $orderPrefix      = $data["groups"]["orderid"]["fields"]["order"]["value"];
        $invoicePrefix    = $data["groups"]["invoiceid"]["fields"]["invoice"]["value"];
        $creditmemoPrefix = $data["groups"]["creditmemoid"]["fields"]["creditmemo"]["value"];
        $shipmentPrefix   = $data["groups"]["shipmentid"]["fields"]["shipment"]["value"];

        if (!empty($orderPrefix)) {
            $this->_changePrefix('order', $orderPrefix, $store);
        }

        if (!empty($invoicePrefix)) {
            $this->_changePrefix('invoice', $invoicePrefix, $store);
        }

        if (!empty($creditmemoPrefix)) {
            $this->_changePrefix('creditmemo', $creditmemoPrefix, $store);
        }

        if (!empty($shipmentPrefix)) {
            $this->_changePrefix('shipment', $shipmentPrefix, $store);
        }
    }
}