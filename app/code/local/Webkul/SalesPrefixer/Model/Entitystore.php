<?php

class Webkul_SalesPrefixer_Model_Entitystore extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init("salesprefixer/entitystore");
    }

}